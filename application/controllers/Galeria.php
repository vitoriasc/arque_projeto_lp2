<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galeria extends CI_Controller {

	public function casamentos(){

		$this->load->view('common/header');
		$this->load->view('galeria/casamentos');
		$this->load->view('common/footer');
    }
    public function arquitetura(){

		$this->load->view('common/header');
		$this->load->view('galeria/arquitetura');
		$this->load->view('common/footer');
    }
    public function infantil(){

		$this->load->view('common/header');
		$this->load->view('galeria/infantil');
		$this->load->view('common/footer');
    }
    public function eventos(){

		$this->load->view('common/header');
		$this->load->view('galeria/eventos');
		$this->load->view('common/footer');
    }
    public function comercial(){

		$this->load->view('common/header');
		$this->load->view('galeria/comercial');
		$this->load->view('common/footer');
    }
    public function cosplay(){

		$this->load->view('common/header');
		$this->load->view('galeria/cosplay');
		$this->load->view('common/footer');
    }
    public function fineart(){

		$this->load->view('common/header');
		$this->load->view('galeria/fineart');
		$this->load->view('common/footer');
    }
    public function gastronomia(){

		$this->load->view('common/header');
		$this->load->view('galeria/gastronomia');
		$this->load->view('common/footer');
    }
    public function moda(){

		$this->load->view('common/header');
		$this->load->view('galeria/moda');
		$this->load->view('common/footer');
    }
}