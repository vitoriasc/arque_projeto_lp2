<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Master extends CI_Controller {

	public function index(){

		$this->load->view('common/header');
		
		$this->load->model('MasterModel');
		$data = $this->MasterModel->get_data();

		$v['carousel_intro'] = $this->load->view('home/carousel_intro_home',$data, true);
		$v['sobre'] = $this->load->view('home/sobre',$data, true);
		$v['local'] = $this->load->view('home/local',$data, true);
		$v['card_carousel'] = $this->load->view('home/card_carousel',$data, true);

		$this->load->view('home/layout_home', $v);

		$this->load->view('common/footer');
	}

	public function contato(){

		$this->load->view('common/header');

		//$this->load->model('MasterModel');
		//$this->MasterModel->envia_contato(); //envia um email, apenas se tiver hospedado

		$this->load->view('contato/contato_layout');
		$this->input->post('nome');

		$this->load->view('common/footer');
	}

	public function teste(){

		$this->load->view('teste');
		
	}
}
