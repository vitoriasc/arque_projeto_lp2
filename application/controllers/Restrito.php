<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Restrito extends CI_Controller {

    public function index(){

        $this->load->view('painelAdm/header_restrito');
        $this->load->view('painelAdm/form_escolhe_home');
		$this->load->view('common/footer');
		
    }

	public function adcionar(){

		$this->load->view('painelAdm/header_restrito');

        $this->load->view('painelAdm/form_cria_home');
        $this->load->model('MasterModel');
		$data = $this->MasterModel->nova_home();

		$this->load->view('common/footer');
	}

	public function editar(){

        $this->load->view('painelAdm/header_restrito');

        $this->load->model('MasterModel');
        $data = $this->MasterModel->get_data(1);
        $this->load->view('painelAdm/form_edita_home', $data);
        $this->MasterModel->edita_home();
        
		$this->load->view('common/footer');
	}


    public function excluir(){

        $this->load->view('painelAdm/header_restrito');

        $this->load->model('MasterModel');
        $data = $this->MasterModel->get_data(1);
       

        $v['itens_tabela'] = $this->load->view('painelAdm/itens_tabela',$data, true);
        
        $this->MasterModel->exclui_home();
        $this->load->view('painelAdm/tabela_exclui_home', $v);

        $this->load->view('common/footer');
		
	}

}