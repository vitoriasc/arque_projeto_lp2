<!DOCTYPE html>
<html lang="pt-br">
    
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Arqué | Estúdio Fotográfico</title>
  <link rel="icon" href="<?= base_url('assets/img/icon.png') ?>">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
 

  <link href="<?= base_url('assets/simplelightbox-master/dist/simplelightbox.min.css')?>" rel='stylesheet' type='text/css'>
  <link href="<?= base_url('assets/simplelightbox-master/demo/demo.css')?>" rel='stylesheet' type='text/css'>
  <link href="<?= base_url('assets/custom.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet" type='text/css'>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light white text-dark fixed-top">
    <a class="navbar-brand pr-3 pt-2 pl-3 " href="http://localhost/arque"><h2>Arqué</h2></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
        aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/arque#sobre">Sobre
            <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link " id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">Galeria</a>
            <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" id="Casamentos" name="Casamentos" href="http://localhost/arque/galeria/casamentos">Casamentos</a>
            <a class="dropdown-item" id="Infantil" name="Infantil" href="http://localhost/arque/galeria/infantil">Infantil</a>
            <a class="dropdown-item" id="Arquitetura" name="Arquitetura" href="http://localhost/arque/galeria/arquitetura">Arquitetura</a>
            <a class="dropdown-item" id="Comercial" name="Comercial" href="http://localhost/arque/galeria/comercial">Comercial</a>
            <a class="dropdown-item" id="Moda" name="Moda" href="http://localhost/arque/galeria/moda">Moda</a>
            <a class="dropdown-item" id="Gastronomia" name="Gastronomia" href="http://localhost/arque/galeria/gastronomia">Gastronomia</a>
            <a class="dropdown-item" id="Cosplay" name="Cosplay" href="http://localhost/arque/galeria/cosplay">Cosplay</a>
            <a class="dropdown-item" id="Eventos" name="Eventos" href="http://localhost/arque/galeria/eventos">Eventos</a>
            <a class="dropdown-item" id="Fine Art" name="Fine Art" href="http://localhost/arque/galeria/fineart">Fine Art</a>
            </div>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/arque/master/contato">Contato</a>
        </li>
        </ul>
        <ul class="navbar-nav ml-auto nav-flex-icons">
        <li class="nav-item">
            <a href="https://www.instagram.com/arque/?hl=pt-br" class="nav-link waves-effect waves-light" target="_blank">
            <i class="fab fa-instagram"></i>
            </a>
        </li>
        <li class="nav-item">
            <a href="https://www.facebook.com/Arqu%C3%A9-Fotos-571087110040928" class="nav-link waves-effect waves-light" target="_blank">
              <i class="fab fa-facebook-f"></i>
            </a>
          </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">
            <i class="fas fa-user"></i>
            </a>
            <div class="dropdown-menu dropdown-menu-right dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" href="http://localhost/arque/restrito">Painel Administrativo</a>
            </div>
        </li>
        </ul>
    </div>
    </nav>
    <!--/.Navbar -->