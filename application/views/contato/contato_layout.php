<br /><br />

<main class="text-center py-5">
    <div class="container wow fadeIn">
        <div class="row">
            <div class="col-md-8 mt-3 view overlay">
                <img src="<?= base_url('assets/img/home/contact.jpg')?>" class="img-fluid " alt="estudio de contato">
                <div class="mask flex-center waves-effect">
                  <p class="white-text">Sempre registrando sonhos</p>
                </div>
            </div>
            <div class="col-md-4">
                <br /><br />
                <form method="POST" class="text-center border border-light p-5">
                    <p class="h4 mb-4">Contate-nos</p>
                    <input type="text" id="nome" name="nome" class="form-control mb-4" placeholder="Nome">
                    <input type="email" id="email" name="email" class="form-control mb-4" placeholder="E-mail">
                    <div class="form-group">
                        <textarea class="form-control rounded-0" id="mensagem" name="mensagem" rows="3" placeholder="Mensagem"></textarea>
                    </div>
                    <button class="btn btn-outline-elegant waves-effect" type="submit">Enviar</button>
                </form>
            </div>
        </div>
    </div>
</main>