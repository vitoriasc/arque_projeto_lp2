<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
    <h1 class="align-center">Arquitetura</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Arquitetura/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Arquitetura/thumb/img1.jpg')?>" alt="Arquitetura" title="Arquitetura" /></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img2.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img2.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img3.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img3.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img4.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img4.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Arquitetura/img5.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img5.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img6.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img6.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Arquitetura/thumb/img7.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img8.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img8.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Arquitetura/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Arquitetura/thumb/img9.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img10.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img10.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img11.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img11.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <a href="<?= base_url('assets/images/img/Arquitetura/img12.jpg')?>"><img src="<?= base_url('assets/img/Arquitetura/thumb/img12.jpg')?>" alt="Arquitetura" title="Arquitetura"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>