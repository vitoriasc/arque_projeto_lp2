<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
    <h1 class="align-center">Casamentos</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Casamentos/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Casamentos/thumb/img1.jpg')?>" alt="Casamentos" title="Casamentos" /></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img2.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img2.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img3.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img3.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img4.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img4.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Casamentos/img5.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img5.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img6.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img6.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Casamentos/thumb/img7.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img8.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img8.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Casamentos/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Casamentos/thumb/img9.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img10.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img10.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img11.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img11.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Casamentos/img12.jpg')?>"><img src="<?= base_url('assets/img/Casamentos/thumb/img12.jpg')?>" alt="Casamentos" title="Casamentos"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>