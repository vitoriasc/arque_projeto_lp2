<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
	<h1 class="align-center">Cosplay</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Cosplay/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Cosplay/thumb/img1.jpg')?>" alt="Cosplay" title="Cosplay" /></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img2.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img2.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img3.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img3.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img4.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img4.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Cosplay/img5.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img5.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img6.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img6.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Cosplay/thumb/img7.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img8.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img8.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Cosplay/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Cosplay/thumb/img9.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img10.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img10.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img11.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img11.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <a href="<?= base_url('assets/images/img/Cosplay/img12.jpg')?>"><img src="<?= base_url('assets/img/Cosplay/thumb/img12.jpg')?>" alt="Cosplay" title="Cosplay"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>