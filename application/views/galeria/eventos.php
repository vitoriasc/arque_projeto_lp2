<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
	<h1 class="align-center">Eventos</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Eventos/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Eventos/thumb/img1.jpg')?>" alt="Eventos" title="Eventos" /></a>
        <a href="<?= base_url('assets/images/img/Eventos/img2.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img2.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img3.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img3.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img4.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img4.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Eventos/img5.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img5.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img6.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img6.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Eventos/thumb/img7.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img8.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img8.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Eventos/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Eventos/thumb/img9.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img10.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img10.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img11.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img11.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <a href="<?= base_url('assets/images/img/Eventos/img12.jpg')?>"><img src="<?= base_url('assets/img/Eventos/thumb/img12.jpg')?>" alt="Eventos" title="Eventos"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>