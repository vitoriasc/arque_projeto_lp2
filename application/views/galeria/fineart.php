<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
    <h1 class="align-center">Fine Art</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Fine Art/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Fine Art/thumb/img1.jpg')?>" alt="Fine Art" title="Fine Art" /></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img2.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img2.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img3.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img3.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img4.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img4.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Fine Art/img5.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img5.jpg')?>" alt="Fine Art" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img6.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img6.jpg')?>" alt="Fine Art" title="Casamentos"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Fine Art/thumb/img7.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img8.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img8.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Fine Art/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Fine Art/thumb/img9.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img10.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img10.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img11.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img11.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <a href="<?= base_url('assets/images/img/Fine Art/img12.jpg')?>"><img src="<?= base_url('assets/img/Fine Art/thumb/img12.jpg')?>" alt="Fine Art" title="Fine Art"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>