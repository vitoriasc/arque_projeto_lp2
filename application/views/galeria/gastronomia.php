<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
	<h1 class="align-center">Gastronomia</h1>	
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Gastronomia/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Gastronomia/thumb/img1.jpg')?>" alt="Gastronomia" title="Gastronomia" /></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img2.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img2.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img3.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img3.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img4.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img4.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Gastronomia/img5.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img5.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img6.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img6.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Gastronomia/thumb/img7.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img8.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img8.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Gastronomia/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Gastronomia/thumb/img9.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img10.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img10.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img11.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img11.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <a href="<?= base_url('assets/images/img/Gastronomia/img12.jpg')?>"><img src="<?= base_url('assets/img/Gastronomia/thumb/img12.jpg')?>" alt="Gastronomia" title="Gastronomia"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>