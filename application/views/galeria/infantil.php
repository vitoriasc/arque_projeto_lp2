<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
	<h1 class="align-center">Infantil</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Infantil/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Infantil/thumb/img1.jpg')?>" alt="Infantil" title="Infantil" /></a>
        <a href="<?= base_url('assets/images/img/Infantil/img2.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img2.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img3.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img3.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img4.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img4.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Infantil/img5.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img5.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img6.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img6.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Infantil/thumb/img7.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img8.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img8.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Infantil/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Infantil/thumb/img9.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img10.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img10.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img11.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img11.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <a href="<?= base_url('assets/images/img/Infantil/img12.jpg')?>"><img src="<?= base_url('assets/img/Infantil/thumb/img12.jpg')?>" alt="Infantil" title="Infantil"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>