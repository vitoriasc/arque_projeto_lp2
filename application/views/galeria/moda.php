<section>
  <br /><br /><br /><br /><br />
    <div class="container wow fadeIn">
    <h1 class="align-center">Moda</h1>
			<br />
      <div class="gallery">

        <a href="<?= base_url('assets/images/img/Moda/img1.jpg')?>" class="big"><img src="<?= base_url('assets/img/Moda/thumb/img1.jpg')?>" alt="Moda" title="Moda" /></a>
        <a href="<?= base_url('assets/images/img/Moda/img2.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img2.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img3.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img3.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img4.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img4.jpg')?>" alt="Moda" title="Moda"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Moda/img5.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img5.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img6.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img6.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img7.jpg')?>" class="big"><img src="<?= base_url('assets/img/Moda/thumb/img7.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img8.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img8.jpg')?>" alt="Moda" title="Moda"/></a>
        <div class="clear"></div>

        <a href="<?= base_url('assets/images/img/Moda/img9.jpg')?>" class="big"><img src="<?= base_url('assets/img/Moda/thumb/img9.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img10.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img10.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img11.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img11.jpg')?>" alt="Moda" title="Moda"/></a>
        <a href="<?= base_url('assets/images/img/Moda/img12.jpg')?>"><img src="<?= base_url('assets/img/Moda/thumb/img12.jpg')?>" alt="Moda" title="Moda"/></a>
        <div class="clear"></div>
            
      </div>	
    </div>
    
</section>