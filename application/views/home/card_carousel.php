<h2 class="my-5 h3 text-center">Galeria</h2>
<div id="multi-item-example" class="carousel slide carousel-multi-item wow fadeIn" data-ride="carousel">

    <div class="controls-top align-content-center">
          <a class="btn-floating" href="#multi-item-example" data-slide="prev"><i class="fas fa-chevron-left"></i></a>
          <a class="btn-floating" href="#multi-item-example" data-slide="next"><i class="fas fa-chevron-right"></i></a>
    </div>

    <ol class="carousel-indicators">
        <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
        <li data-target="#multi-item-example" data-slide-to="1"></li>
        <li data-target="#multi-item-example" data-slide-to="2"></li>
    </ol>

    <div class="carousel-inner row" role="listbox">

        <div class="carousel-item active">
            <div class="row">
                <div class="col-md-4">
                    <div class="card mb-2">
                        <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card1.'.jpg')?>"
                        alt="Card image cap">
                        <div class="card-body">
                        <h4 class="card-title"> <?= $card_title1 ?> </h4>
                        <p class="card-text"> <?= $conteudo_card1 ?> </p>
                        <a  href="http://localhost/arque/galeria/<?= $card_title1 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                    </div>
                </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card2.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title2 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card2 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title2 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card3.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title3 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card3 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title3 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="carousel-item">
        <div class ="row">
            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card4.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title4 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card4 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title4 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card5.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title5 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card5 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title5 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card6.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title6 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card6 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title6 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>
        </div>
    </div>

    <div class="carousel-item">
        <div class="row">
            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card7.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title7 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card7 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title7 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <div class="card mb-2">
                <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card8.'.jpg')?>"
                  alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title"> <?= $card_title8 ?>  </h4>
                  <p class="card-text"> <?= $conteudo_card8 ?> </p>
                  <a href="http://localhost/arque/galeria/<?= $card_title8 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                </div>
              </div>
            </div>

            <div class="col-md-4">
                <div class="card mb-2">
                    <img class="card-img-top" src="<?= base_url('assets/img/home/card/img'.$img_card9.'.jpg')?>" alt="Card image cap">
                        <div class="card-body">
                            <h4 class="card-title"> <?= $card_title9 ?>  </h4>
                            <p class="card-text"> <?= $conteudo_card9 ?> </p>
                            <a href="http://localhost/arque/galeria/<?= $card_title9 ?>" class="btn btn-outline-elegant waves-effect">Veja Mais</a>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>