<ol class="carousel-indicators">
    <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
    <li data-target="#carousel-example-1z" data-slide-to="1"></li>
    <li data-target="#carousel-example-1z" data-slide-to="2"></li>
</ol>

  <div class="carousel-inner" role="listbox">

    <div class="carousel-item active">
        <div class="view" style="background-image: url('<?= base_url('assets/img/home/img_carousel'.$img_carousel_intro1.'.jpg')?>'); background-repeat: no-repeat; background-size: cover;">
            <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
                <div class="text-center white-text mx-5 wow fadeIn">
                    <h1 class="mb-4">
                    <strong>Arqué</strong>
                    </h1>
                    <p>
                    <strong>Estúdio Fotográfico</strong>
                    </p>
                    <p class="mb-4 d-none d-md-block">
                    <strong> <?= $frase_intro ?> </strong>
                    </p>
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="carousel-item">
        <div class="view" style="background-image: url('<?= base_url('assets/img/home/img_carousel'.$img_carousel_intro2.'.jpg')?>'); background-repeat: no-repeat; background-size: cover;">
            <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
                <div class="text-center white-text mx-5 wow fadeIn">
                <h1 class="mb-4">
                    <strong>Arqué</strong>
                </h1>

                <p>
                    <strong>Estúdio Fotográfico</strong>
                </p>

                <p class="mb-4 d-none d-md-block">
                    <strong><?= $frase_intro ?></strong>
                </p>
                </div>
            </div>
        </div>
    </div>

    <div class="carousel-item">
        <div class="view" style="background-image: url('<?= base_url('assets/img/home/img_carousel'.$img_carousel_intro3.'.jpg')?>'); background-repeat: no-repeat; background-size: cover;">
            <div class="mask rgba-black-light d-flex justify-content-center align-items-center">
                <div class="text-center white-text mx-5 wow fadeIn">
                <h1 class="mb-4">
                <strong>Arqué</strong>
                </h1>

                <p>
                    <strong>Estúdio Fotográfico</strong>
                </p>

                <p class="mb-4 d-none d-md-block">
                    <strong><?= $frase_intro ?> </strong>
                </p>
            </div>
        </div>
    </div>

  </div>

  <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>