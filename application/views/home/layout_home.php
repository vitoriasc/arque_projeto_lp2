
<div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
    <?= $carousel_intro ?>
</div>

<br id="sobre" /><br />

<div class="container">

  <section class="mt-5 wow fadeIn ">
    <?= $sobre ?>
  </section>
 
  <hr class="my-5">
  
  <section>
    <?= $local ?>
  </section>

  <hr class="mb-5">

  <section>
    <?= $card_carousel ?>
  </section>


</div>
<br /><br /><br />

