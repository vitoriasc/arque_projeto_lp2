<h2 class="my-5 h3 text-center">Onde estamos</h2>
<div class="row ">
    <div class="col-5 wow fadeIn">
        <p><strong>Rua Cachoeira, 625
        <br />
        Edifício Upper Comercial - Sala 105
        <br />
        Guarulhos - São Paulo</strong>
        </p>
        <br />
        <p>Agende uma visita conosco e vamos tomar um café e conversar melhor sobre todos os detalhes do seu ensaio. Será um prazer para nós recebê-los!</p>
        <br />
        <a class="btn btn-outline-elegant waves-effect" href="http://localhost/arque/master/contato">Faça seu orçamento</a>
    </div>
    <div class="col-5 ">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3660.4418186205007!2d-46.550714485190845!3d-23.44452248663353!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cef5024b99db1d%3A0x77fda259ff08824!2sEst%C3%BAdio+Samuel+Angelo+-+Foto+Cachoeira!5e0!3m2!1spt-BR!2sbr!4v1551888155274" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
    </div>
</div>
<br />