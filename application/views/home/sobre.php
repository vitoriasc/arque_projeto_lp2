<div class="row">

      <div class="col-md-6 mb-4">
        <img src="<?= base_url('assets/img/home/'.$img_sobre.'.jpg')?>" class="img-fluid z-depth-1-half" alt="estudio">
      </div>

      <div class="col-md-6 mb-4">
        <h3 class="h3 mb-4"> <?= $titulo_sobre ?> </h3>
        <br />
        <p> <?= $conteudo_sobre1 ?> </p>

        <hr />

        <p> <?= $conteudo_sobre2 ?> </P>
      </div>
</div>