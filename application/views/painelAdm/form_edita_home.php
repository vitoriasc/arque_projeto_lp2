<div class="row">
        <div class="col-md-6 mx-auto mt-3 mb-4 wow fadeIn">
            <form method="POST" class="text-center border border-light p-5">

            <br /><br />
            <p class="h3 mb-4">Edita Home</p>

            <br /><br />
            <p class="h5 mb-4">Introducao</p>
            <input type="text" value="<?= isset($frase_intro) ? $frase_intro: '' ?>" id="frase_intro" name="frase_intro" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_carousel_intro1) ? $img_carousel_intro1: '' ?>" id="img_carousel_intro1" name="img_carousel_intro1" class="form-control mb-4" placeholder="Numero da primeira imagem">
            <input type="number" value="<?= isset($img_carousel_intro2) ? $img_carousel_intro2: '' ?>" id="img_carousel_intro2" name="img_carousel_intro2" class="form-control mb-4" placeholder="Numero da segunda imagem">
            <input type="number" value="<?= isset($img_carousel_intro3) ? $img_carousel_intro3: '' ?>" id="img_carousel_intro3" name="img_carousel_intro3" class="form-control mb-4" placeholder="Numero da terceira imagem">

            <br /><br />
            <p class="h5 mb-4">Sobre</p>
            <input type="text" value="<?= isset($titulo_sobre) ? $titulo_sobre: '' ?>" id="titulo_sobre" name="titulo_sobre" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($img_sobre) ? $img_sobre: '' ?>" id="img_sobre" name="img_sobre" class="form-control mb-4" placeholder="Nome da imagem">
            <div class="form-group">
                <textarea class="form-control rounded-0" id="conteudo_sobre1" name="conteudo_sobre1" rows="3" placeholder="Paragrafo 1"><?= isset($conteudo_sobre1) ? $conteudo_sobre1: '' ?></textarea>
            </div>
            <div class="form-group">
                <textarea class="form-control rounded-0" id="conteudo_sobre2" name="conteudo_sobre2" rows="3" placeholder="Paragrafo 2"><?= isset($conteudo_sobre1) ? $conteudo_sobre1: '' ?></textarea>
            </div>

            <br /><br />
            <p class="h5 mb-4">Cartoes da galeria</p>
            <br />
            <p class="h6 mb-4">Cartao 1</p>
            <input type="text" value="<?= isset($card_title1) ? $card_title1: '' ?>" id="card_title1" name="card_title1" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card1) ? $conteudo_card1: '' ?>" id="conteudo_card1" name="conteudo_card1" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card1) ? $img_card1: '' ?>" id="img_card1" name="img_card1" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 2</p>
            <input type="text" value="<?= isset($card_title2) ? $card_title2: '' ?>" id="card_title2" name="card_title2" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card2) ? $conteudo_card2: '' ?>" id="conteudo_card2" name="conteudo_card2" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card2) ? $img_card2: '' ?>" id="img_card2" name="img_card2" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 3</p>
            <input type="text" value="<?= isset($card_title3) ? $card_title3: '' ?>" id="card_title3" name="card_title3" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card3) ? $conteudo_card3: '' ?>" id="conteudo_card3" name="conteudo_card3" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card3) ? $img_card3: '' ?>" id="img_card3" name="img_card3" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 4</p>
            <input type="text" value="<?= isset($card_title4) ? $card_title4: '' ?>" id="card_title4" name="card_title4" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card4) ? $conteudo_card4: '' ?>" id="conteudo_card4" name="conteudo_card4" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card4) ? $img_card4: '' ?>" id="img_card4" name="img_card4" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 5</p>
            <input type="text" value="<?= isset($card_title5) ? $card_title5: '' ?>" id="card_title5" name="card_title5" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card5) ? $conteudo_card5: '' ?>" id="conteudo_card5" name="conteudo_card5" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card5) ? $img_card5: '' ?>" id="img_card5" name="img_card5" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 6</p>
            <input type="text" value="<?= isset($card_title6) ? $card_title6: '' ?>" id="card_title6" name="card_title6" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card6) ? $conteudo_card6: '' ?>" id="conteudo_card6" name="conteudo_card6" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card6) ? $img_card6: '' ?>" id="img_card6" name="img_card6" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 7</p>
            <input type="text" value="<?= isset($card_title7) ? $card_title7: '' ?>" id="card_title7" name="card_title7" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card7) ? $conteudo_card7: '' ?>" id="conteudo_card7" name="conteudo_card7" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card7) ? $img_card7: '' ?>" id="img_card7" name="img_card7" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 8</p>
            <input type="text" value="<?= isset($card_title8) ? $card_title8: '' ?>" id="card_title8" name="card_title8" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card8) ? $conteudo_card8: '' ?>" id="conteudo_card8" name="conteudo_card8" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card8) ? $img_card8: '' ?>" id="img_card8" name="img_card8" class="form-control mb-4" placeholder="Numero da imagem">

            <p class="h6 mb-4">Cartao 9</p>
            <input type="text" value="<?= isset($card_title9) ? $card_title9: '' ?>" id="card_title9" name="card_title9" class="form-control mb-4" placeholder="Titulo">
            <input type="text" value="<?= isset($conteudo_card9) ? $conteudo_card9: '' ?>" id="conteudo_card9" name="conteudo_card9" class="form-control mb-4" placeholder="Frase">
            <input type="number" value="<?= isset($img_card9) ? $img_card9: '' ?>" id="img_card9" name="img_card9" class="form-control mb-4" placeholder="Numero da imagem">


            <br /><br />
            <button class="btn btn-outline-dark btn-block" type="submit">Enviar</button>

            </form>
        </div>
    </div>
</div>