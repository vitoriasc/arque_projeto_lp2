<!DOCTYPE html>
<html lang="pt-br">
    
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <title>Arqué | Estúdio Fotográfico</title>
  <link rel="icon" href="<?= base_url('assets/img/icon.png') ?>">

  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
 

  <link href="<?= base_url('assets/simplelightbox-master/dist/simplelightbox.min.css')?>" rel='stylesheet' type='text/css'>
  <link href="<?= base_url('assets/simplelightbox-master/demo/demo.css')?>" rel='stylesheet' type='text/css'>
  <link href="<?= base_url('assets/custom.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/custom_galeria.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/mdb/css/bootstrap.min.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/mdb/css/mdb.min.css') ?>" rel="stylesheet" type='text/css'>
  <link href="<?= base_url('assets/mdb/css/style.css') ?>" rel="stylesheet" type='text/css'>
</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light white text-dark fixed-top">
    <a class="navbar-brand pr-3 pt-2 pl-3 " href="http://localhost/arque"><h2>Arqué</h2></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent-333"
        aria-controls="navbarSupportedContent-333" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent-333">
        <ul class="navbar-nav mr-auto">
        <li class="nav-item">
            <a class="nav-link" href="http://localhost/arque#sobre">
            <span class="sr-only">(current)</span>
            </a>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link " id="navbarDropdownMenuLink-333" data-toggle="dropdown" aria-haspopup="true"
            aria-expanded="false">Home</a>
            <div class="dropdown-menu dropdown-default" aria-labelledby="navbarDropdownMenuLink-333">
            <a class="dropdown-item" id="Escolher" name="Escolher" href="http://localhost/arque/restrito">Escolher</a>
            <a class="dropdown-item" id="Adicionar" name="Adicionar" href="http://localhost/arque/restrito/adcionar">Adicionar</a>
            <a class="dropdown-item" id="Editar" name="Editar" href="http://localhost/arque/restrito/editar">Editar</a>
            <a class="dropdown-item" id="Excluir" name="Excluir" href="http://localhost/arque/restrito/excluir">Excluir</a>
            </div>
        </li>
        </ul>
        <ul class="navbar-nav ml-auto ">
        <li class="nav-item">
            <h5>Painel Administrativo</h5>
        </li>
        </ul>
    </div>
    </nav>
    <!--/.Navbar -->