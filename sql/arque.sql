-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11-Mar-2019 às 03:20
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `arque`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tabela_galeria`
--

CREATE TABLE `tabela_galeria` (
  `id` tinyint(5) NOT NULL,
  `frase_intro` text NOT NULL,
  `img_carousel_intro1` tinyint(4) NOT NULL,
  `img_carousel_intro2` tinyint(4) NOT NULL,
  `img_carousel_intro3` tinyint(4) NOT NULL,
  `img_sobre` varchar(20) NOT NULL,
  `titulo_sobre` varchar(60) NOT NULL,
  `conteudo_sobre1` varchar(600) NOT NULL,
  `conteudo_sobre2` varchar(600) NOT NULL,
  `card_title1` varchar(60) NOT NULL,
  `card_title2` varchar(60) NOT NULL,
  `card_title3` varchar(60) NOT NULL,
  `card_title4` varchar(60) NOT NULL,
  `card_title5` varchar(60) NOT NULL,
  `card_title6` varchar(60) NOT NULL,
  `card_title7` varchar(60) NOT NULL,
  `card_title8` varchar(60) NOT NULL,
  `card_title9` varchar(60) NOT NULL,
  `conteudo_card1` varchar(200) NOT NULL,
  `conteudo_card2` varchar(200) NOT NULL,
  `conteudo_card3` varchar(200) NOT NULL,
  `conteudo_card4` varchar(200) NOT NULL,
  `conteudo_card5` varchar(200) NOT NULL,
  `conteudo_card6` varchar(200) NOT NULL,
  `conteudo_card7` varchar(200) NOT NULL,
  `conteudo_card8` varchar(200) NOT NULL,
  `conteudo_card9` varchar(200) NOT NULL,
  `img_card1` tinyint(4) NOT NULL,
  `img_card2` tinyint(4) NOT NULL,
  `img_card3` tinyint(4) NOT NULL,
  `img_card4` tinyint(4) NOT NULL,
  `img_card5` tinyint(4) NOT NULL,
  `img_card6` tinyint(4) NOT NULL,
  `img_card7` tinyint(4) NOT NULL,
  `img_card8` tinyint(4) NOT NULL,
  `img_card9` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `tabela_galeria`
--

INSERT INTO `tabela_galeria` (`id`, `frase_intro`, `img_carousel_intro1`, `img_carousel_intro2`, `img_carousel_intro3`, `img_sobre`, `titulo_sobre`, `conteudo_sobre1`, `conteudo_sobre2`, `card_title1`, `card_title2`, `card_title3`, `card_title4`, `card_title5`, `card_title6`, `card_title7`, `card_title8`, `card_title9`, `conteudo_card1`, `conteudo_card2`, `conteudo_card3`, `conteudo_card4`, `conteudo_card5`, `conteudo_card6`, `conteudo_card7`, `conteudo_card8`, `conteudo_card9`, `img_card1`, `img_card2`, `img_card3`, `img_card4`, `img_card5`, `img_card6`, `img_card7`, `img_card8`, `img_card9`) VALUES
(1, '“Fotografar é conseguir captar o que existe atrás do que se vê com os olho, é ver através de uma parede invisível.”', 1, 2, 3, 'estudio', 'O estúdio que realiza sonhos', 'O estúdio Arqué é formado por duas irmãs apaixonadas por arte, \r\n                                    fotografia e cultura, que pensaram cuidadosamente em elaborar um \r\n                                    espaço onde a fotográfia fosse vista além da captura de imagens.', 'O estúdio Arqué é formado por duas irmãs apaixonadas por arte, \r\n                                    fotografia e cultura, que pensaram cuidadosamente em elaborar um \r\n                                    espaço onde a fotográfia fosse vista além da captura de imagens.', 'Casamentos', 'Infantil', 'Arquitetura', 'Comercial', 'Moda', 'Gastronomia', 'Cosplay', 'Eventos', 'Fine Art', 'Caputura do momento mais especial a dois', 'Registro de momentos unicos na vida', 'Fotos de elegancia, design e conforto', 'Empresas fotogradas por dentro e fora', 'Registros de arte com estilo', 'A comida mais saborosa se come com os olhos', 'Fotos de sonhos representados por fantasias', 'Os melhoes clicks dos melhores momentos', 'A arte presente nos penos detalhes do dia a dia', 1, 2, 3, 4, 5, 6, 7, 8, 9);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tabela_galeria`
--
ALTER TABLE `tabela_galeria`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tabela_galeria`
--
ALTER TABLE `tabela_galeria`
  MODIFY `id` tinyint(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
